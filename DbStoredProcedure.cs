﻿using System;
using System.Collections.Generic;

namespace Empiria.Data.Modeler {

  public class DbStoredProcedure : DbQueryBase {
      
    #region Constructors and parsers

    private DbStoredProcedure(string databaseName): base(databaseName) {
    }

    public static DbStoredProcedure Parse(string databaseName){
      var dbSp = new DbStoredProcedure(databaseName);
      return dbSp;
    }

    #endregion Constructors and parsers

  }
}
