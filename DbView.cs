﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Empiria.Data.Modeler {

  public class DbView : DbTableBase {

    #region Fields

    DbDataSource dbDataSource;   

    #endregion Fields

    #region Constructors and parsers

    private DbView(string databaseName): base(databaseName) {
      this.TableType = DbTableType.dbView;
      dbDataSource = DbDataSource.Parse(databaseName);
    }

    public static DbView Parse(string databaseName){
      var dbView = new DbView(databaseName);
      return dbView;
    }

    #endregion

    #region Public properties

    public string SqlCode {
      get { return GetSqlCode(); }
    }

    #endregion

    #region Protected methods

    protected override int GetCountColumns() {
      return dbDataSource.CountViewColumns(this.TableName);
    }

    #endregion

    #region Private methdos

    private string GetSqlCode() {
      return dbDataSource.GetObjectCode(this.TableName);
    }
    #endregion

    #region Public methods      
          
    #endregion Public methods

  } //DbView
} //Empiria.Data.Modeler
