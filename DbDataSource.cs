﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Empiria.Data.Modeler {

  public class DbDataSource {
      
    #region Constructors and parsers

    private DbDataSource(string databaseName) {
      this.DatabaseName = databaseName;

    }

    public static DbDataSource Parse(string dataBaseName) {
      var dbDataSoruce = new DbDataSource(dataBaseName);
      return dbDataSoruce;
    }

    #endregion Constructors and parsers

    #region Public propierties

    public string DatabaseName {
      get;
      private set;
    }


    #endregion Public propierties
    
    #region Tables and Views

    public DataTable GetDataBaseTables() {
      var op = DataOperation.Parse("@qryDBTables", this.DatabaseName);

      return DataReader.GetDataTable(op);
    }

    public int CountTableRows(string tableName) {
      var op = DataOperation.Parse("@getDBTableRowsCount", this.DatabaseName, tableName);
      object totalRows = DataReader.GetFieldValue(op, "TotalRows");

      return  Convert.ToInt32(totalRows);              
    }

    public int CountTableColumns(string tableName) {
      var op = DataOperation.Parse("@getDBTableColumnsCount", this.DatabaseName, tableName);
      object totalColumns = DataReader.GetFieldValue(op, "TotalColumns");

      return Convert.ToInt32(totalColumns);         
    }

    private DataTable GetDatabaseViews() {
      var op = DataOperation.Parse("@qryDBVIews", this.DatabaseName);

      return DataReader.GetDataTable(op);      
    }

    public int CountViewColumns(string viewName) {
      var op = DataOperation.Parse("@getDBViewColumnsCount", this.DatabaseName, viewName);
      object totalColumns = DataReader.GetFieldValue(op, "TotalColumns");

      return Convert.ToInt32(totalColumns);
    }

    #endregion Tables and Views

    #region Get object code

    public string GetObjectCode(string objectName) {
      var op = DataOperation.Parse("@getDBObjectCode", this.DatabaseName, objectName);
      object code = DataReader.GetFieldValue(op, "Code");

      return code.ToString();      
    }

    #endregion Get object code

    #region Fields

    private DataTable GetTableFields(string tableName) {
      var op = DataOperation.Parse("@qryDBTableFileds", this.DatabaseName, tableName);

      return DataReader.GetDataTable(op);     
    }

    private DataTable GetViewFields(string viewName) {
      var op = DataOperation.Parse("@qryDBViewFileds", this.DatabaseName, viewName);
     
      return DataReader.GetDataTable(op);    
    }

    public DataTable GetPrimaryKeys(string tableName) {
      var op = DataOperation.Parse("@qryDBTablePrimaryKeys", this.DatabaseName, tableName);      

      return DataReader.GetDataTable(op);      
    }

    #endregion Fields

    #region index

    private DataTable GetDataBaseIndexes() {
      var op = DataOperation.Parse("@qryDBIndexes", this.DatabaseName);

      return DataReader.GetDataTable(op);     
    }

    private DataTable GetDBTableIndex(string tableName) {
      var op = DataOperation.Parse("@qryDBTableIndexes", this.DatabaseName, tableName);
      
      return DataReader.GetDataTable(op);    
    }        

    #endregion

    #region Dependencies

    private DataTable GetTableDependences(string TableName) {
      var op = DataOperation.Parse("@qryDBTableDependencies", this.DatabaseName, TableName);
      
      return DataReader.GetDataTable(op);
    }
    
    #endregion

    #region Functions and StoredProcedures

    private DataTable GetDatabaseStoredProcedures() {
      var op = DataOperation.Parse("@qryDBStoredProcedures", this.DatabaseName);      

      return DataReader.GetDataTable(op);    
    }

    public DataTable GetDatabaseFunctions() {
      var op = DataOperation.Parse("@qryDBFunctions", this.DatabaseName);      

      return DataReader.GetDataTable(op);     
    }

    #endregion

    #region Parameter

    private DataTable dtParamTable() {
      DataTable dtPTable = new DataTable();
      DataColumn dcIndex = new DataColumn("Index", typeof(int));
      DataColumn dcName = new DataColumn("Name", typeof(string));
      DataColumn dcType = new DataColumn("Type", typeof(string));
      DataColumn dcPrecision = new DataColumn("Precision", typeof(string));
      DataColumn dcDecimal = new DataColumn("Scale", typeof(string));
      DataColumn dcSize = new DataColumn("Size", typeof(string));
      DataColumn dcDirection = new DataColumn("Direction", typeof(string));
      dtPTable.Columns.Add(dcIndex);
      dtPTable.Columns.Add(dcName);
      dtPTable.Columns.Add(dcType);
      dtPTable.Columns.Add(dcPrecision);
      dtPTable.Columns.Add(dcDecimal);
      dtPTable.Columns.Add(dcSize);
      dtPTable.Columns.Add(dcDirection);
      return dtPTable;
    }

    private DataTable GetParameters(string queryName) {
      string connectionString = "Server=CHRIS-PC\\SQLEXPRESS;Database=Zacatecas;Trusted_Connection=True;";
      SqlConnection scConnect = new SqlConnection(connectionString);
      SqlCommand scParams = new SqlCommand();
      scParams.Connection = scConnect;
      scParams.CommandText = queryName;
      scParams.CommandType = CommandType.StoredProcedure;
      scParams.Connection.Open();
      SqlCommandBuilder.DeriveParameters(scParams);
      scParams.Connection.Close();
      int intCountParameters = scParams.Parameters.Count;
      DataTable dtParamsTable = dtParamTable();
      for (int i = 0; i < intCountParameters; i++) {
        if (scParams.Parameters[i].Direction == ParameterDirection.Input) {
          DataRow Row = dtParamsTable.NewRow();
          Row["Index"] = i;
          Row["Name"] = scParams.Parameters[i].ParameterName.ToString();
          Row["Type"] = (int) scParams.Parameters[i].SqlDbType;
          Row["Precision"] = scParams.Parameters[i].Precision.ToString();
          Row["Scale"] = scParams.Parameters[i].Scale.ToString();
          Row["Size"] = scParams.Parameters[i].Size.ToString();
          Row["Direction"] = (int) scParams.Parameters[i].Direction;
          dtParamsTable.Rows.Add(Row);
        }
      }
      return dtParamsTable;
    }

    #endregion

    #region GetTables and Views

    public List<DbTable> GetTables() {

      DataTable tables = GetDataBaseTables();
      List<DbTable> tableList = new List<DbTable>();
      foreach (DataRow row in tables.Rows) {
        var table = DbTable.Parse(this.DatabaseName);
        table.TableName = row["name"].ToString();
        table.CreateDate = Convert.ToDateTime(row["create_date"]);
        table.ModifyDate = Convert.ToDateTime(row["modify_date"]);
        tableList.Add(table);
      }
      return tableList;
    }

    public List<DbTable> GetTables(string[] exceptions) {
      int index = 0;
      List<DbTable> tables = GetTables();
      foreach (string exception in exceptions) {
        index = tables.FindIndex(table => table.TableName == exception);
        if (index != -1) {
          tables.RemoveAt(index);
        } else {
          //no-op
        }
      }
      return tables;
    }

    #region Views

    public List<DbView> GetViews() {

      DataTable views = GetDatabaseViews();
      List<DbView> viewList = new List<DbView>();
      foreach (DataRow row in views.Rows) {
        var table = DbView.Parse(this.DatabaseName);
        table.TableName = row["name"].ToString();
        table.CreateDate = Convert.ToDateTime(row["create_date"]);
        table.ModifyDate = Convert.ToDateTime(row["modify_date"]);
        viewList.Add(table);
      }
      return viewList;
    }

    public List<DbView> GetViews(string[] exceptions) {
      int index = 0;
      List<DbView> views = GetViews();
      foreach (string exception in exceptions) {
        index = views.FindIndex(table => table.TableName == exception);
        if (index != -1) {
          views.RemoveAt(index);
        } else {
          //no-op
        }
      }
      return views;
    }

    #endregion Views


    #endregion GetTables and Views

    #region GetFields

    private DataTable GetFieldsTable(string tableName, DbTableBase.DbTableType tableType) {
      DataTable FieldsTable = new DataTable();
      switch (tableType) {
        case DbTableBase.DbTableType.dbTable: {
            FieldsTable = GetTableFields(tableName);
          } break;
        case DbTableBase.DbTableType.dbView: {
            FieldsTable = GetViewFields(tableName);
          } break;
      }
      return FieldsTable;
    }

    public List<DbField> GetFields(string tableName, DbTableBase.DbTableType tableType) {
      List<DbField> FieldList = new List<DbField>();
      DataTable fieldTable = GetFieldsTable(tableName, tableType);
      foreach (DataRow row in fieldTable.Rows) {
        var field = DbField.Parse(this.DatabaseName, tableName, tableType);
        field.Name = row["FieldName"].ToString();
        field.Index = Convert.ToInt32(row["FieldId"]);
        field.Collation = row["Collation"].ToString();
        field.DefaultValue = row["DefaultValue"].ToString();
        field.DataType = Convert.ToInt32(row["DataType"]);
        field.MaxLength = Convert.ToInt32(row["MaxLength"]);
        field.Precision = Convert.ToInt32(row["Precision"]);
        field.Scale = Convert.ToInt32(row["Scale"]);
        field.IsIdentity = (bool) row["IsIdentity"];
        field.IsComputed = (bool) row["IsComputed"];
        field.IsNullable = (bool) row["IsNullable"];
        field.IsReplicated = (bool) row["IsReplicated"];
        FieldList.Add(field);
      }
      return FieldList;
    }



    #endregion GetFields

    #region GetIndexes

    public List<DbIndex> GetTableIndex(string tableName) {
      List<DbIndex> indexList = new List<DbIndex>();
      DataTable indexTable = GetDBTableIndex(tableName);
      foreach (DataRow row in indexTable.Rows) {
        var index = DbIndex.Parse(this.DatabaseName, tableName);
        index.Name = row["IndexName"].ToString();
        index.Type = row["IndexType"].ToString();
        indexList.Add(index);
      }
      return indexList;
    }

    #endregion GetIndexes

    #region GetDependencies

    public List<DbDependencies> GetTableDependencies(string tableName) {
      var references = new List<DbDependencies>();
      DataTable referencesTable = GetTableDependences(tableName);
      foreach (DataRow row in referencesTable.Rows) {
        var dbDependence = DbDependencies.Parse(this.DatabaseName, tableName);
        dbDependence.DependentObject = row["DependenceName"].ToString();
        dbDependence.Description = row["EntityDescription"].ToString();
        references.Add(dbDependence);
      }
      return references;
    }

    #endregion GetDependencies

    #region GetStoredProcedures

    public List<DbStoredProcedure> GetStoredProcedures() {
      List<DbStoredProcedure> storedProceuresList = new List<DbStoredProcedure>();
      DataTable storedProceduresTable = GetDatabaseStoredProcedures();
      foreach (DataRow row in storedProceduresTable.Rows) {
        var storedProcedure = DbStoredProcedure.Parse(this.DatabaseName);
        storedProcedure.QueryName = row["Name"].ToString();
        storedProcedure.CreateDate = (DateTime) row["CreateDate"];
        storedProcedure.ModifyDate = (DateTime) row["ModifyDate"];
        storedProceuresList.Add(storedProcedure);
      }
      return storedProceuresList;
    }

    public List<DbStoredProcedure> GetStoredProcedures(string[] exceptions) {
      int index = 0;
      List<DbStoredProcedure> storedProceduresList = GetStoredProcedures();
      foreach (string exception in exceptions) {
        index = storedProceduresList.FindIndex(storedProcedure => storedProcedure.QueryName == exception);
        if (index != -1) {
          storedProceduresList.RemoveAt(index);
        } else {
          //no-op
        }
      }
      return storedProceduresList;
    }

    #endregion GetStoredProcedures

    #region GetFuncttions

    public List<DbFunction> GetFunctions() {
      DataTable functionsTable = GetDatabaseFunctions();
      List<DbFunction> functionsList = new List<DbFunction>();
      foreach (DataRow row in functionsTable.Rows) {
        var function = DbFunction.Parse(this.DatabaseName);
        function.QueryName = row["Name"].ToString();
        function.Type = row["Type"].ToString();
        function.CreateDate = Convert.ToDateTime(row["CreateDate"]);
        function.ModifyDate = Convert.ToDateTime(row["ModifyDate"]);
        functionsList.Add(function);
      }
      return functionsList;
    }

    public List<DbFunction> GetFunctions(string[] exceptions) {
      int index = 0;
      List<DbFunction> FunctionList = GetFunctions();
      foreach (string exception in exceptions) {
        index = FunctionList.FindIndex(storedProcedure => storedProcedure.QueryName == exception);
        if (index != -1) {
          FunctionList.RemoveAt(index);
        } else {
          //no-op
        }
      }
      return FunctionList;
    }

    #endregion GetFunctions

    #region GetQueryParameters

    public List<DbQueryParameter> GetQueryParameters(string queryName) {
      List<DbQueryParameter> queryParameters = new List<DbQueryParameter>();
      DataTable queryParametersTable = GetParameters(queryName);
      foreach (DataRow row in queryParametersTable.Rows) {
        var queryParameter = DbQueryParameter.Parse(queryName);
        queryParameter.Index = (int) row["Index"];
        queryParameter.Name = row["Name"].ToString();
        queryParameter.SqlDbType = Convert.ToInt32(row["Type"]);
        queryParameter.Direction = Convert.ToInt32(row["Direction"]);
        queryParameter.Size = Convert.ToInt32(row["Size"]);
        queryParameter.Scale = Convert.ToInt32(row["Scale"]);
        queryParameter.Precision = Convert.ToInt32(row["Precision"]);
        queryParameter.DefaultValue = string.Empty;
        queryParameters.Add(queryParameter);
      }
      return queryParameters;
    }

    #endregion GetQueryParameters

  }
}
