﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Empiria.Data.Modeler {

  public class DbTable : DbTableBase {

    #region Fields
    
    private DbDataSource dbDataSource ;

    #endregion Fields

    #region Constructors and parsers

    private DbTable(string dataBaseName): base(dataBaseName) {
      this.TableType = DbTableType.dbTable;
      dbDataSource = DbDataSource.Parse(this.DatabaseName);
    }

    public static DbTable Parse(string dataBaseName){
      var dbTable = new DbTable(dataBaseName);
      return dbTable;
    }

    #endregion Constructors and parsers

    #region Public properties

    public int RowsCount {
      get { return dbDataSource.CountTableRows(this.TableName); }
    }

    #endregion Public properties

    #region Protected methods

    protected override int GetCountColumns() {
      return dbDataSource.CountTableColumns(this.TableName);
    }

    #endregion

    #region Private methods

    #endregion

    #region Public methods
      
    public DbIndex[] GetIndexes() {
      return dbDataSource.GetTableIndex(this.TableName).ToArray(); 
    }

    #endregion Public methods

  } //DbTable
} //Empiria.Data.Modeler
