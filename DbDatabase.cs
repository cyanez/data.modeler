﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Empiria.Data.Modeler {
  public class DbDatabase {
  
  private DbDataSource  dbDataSource;

    #region Constructors and parsers
        
    private DbDatabase(string databaseName){
      this.DatabaseName = databaseName;
      dbDataSource = DbDataSource.Parse(this.DatabaseName);
    }    

    public static DbDatabase Parse(string dataBaseName){
      var dbDatabase = new DbDatabase(dataBaseName);
      return dbDatabase;
    }           
     
    #endregion Constructors and parsers

    #region Public properties

    public string DatabaseName{
      get;
      private set;
    }
      
    #endregion Public properties

    #region Public Methods

    public DbTable[] GetTables(){
      return dbDataSource.GetTables().ToArray();
    }

    public DbTable[] GetTables(string[] exceptions){
      return dbDataSource.GetTables(exceptions).ToArray();
    }

    public DbView[] GetViews() {
      return dbDataSource.GetViews().ToArray();
    }

    public DbView[] GetViews(string[] exceptions){
      return dbDataSource.GetViews(exceptions).ToArray();
    }

    public DbStoredProcedure[] GetStoredProcedures(){
      return dbDataSource.GetStoredProcedures().ToArray(); 
    }   

    public DbStoredProcedure[] GetStoredProcedures(string[] exceptions) {
      return dbDataSource.GetStoredProcedures(exceptions).ToArray();
    }

    public DbFunction[] GetFunctions(){
      return dbDataSource.GetFunctions().ToArray(); 
    }

    public DbFunction[] GetFunctions(string[] exceptions) {
      return dbDataSource.GetFunctions(exceptions).ToArray();
    }

    #endregion Public Methods

  } //DbDatabase
} //Empiria.Data.Modeler 
